package com.pavelluzhetskiy.randomringtone

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.pavelluzhetskiy.randomringtone.core.PermissionsManager
import com.pavelluzhetskiy.randomringtone.core.RingtonePreferences
import com.pavelluzhetskiy.randomringtone.workers.SetRandomRingtoneWorker.Companion.enqueueTimer

class MainActivity : AppCompatActivity() {
    val permissionsManager = PermissionsManager()
    val ringtonePreferences = RingtonePreferences()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
    }

    override fun onStart() {
        super.onStart()
        if (!permissionsManager.checkSettingsPermission(this)) {
            Navigation.findNavController(this, R.id.navHost).navigate(R.id.action_global_welcomeFragment)
        } else {
            val timerType = ringtonePreferences.getTimerSwitch(this)
            enqueueTimer(this, timerType)
        }
    }

    //    public void pickRingtone(){
    ////        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
    ////        intent.setType("file/audio");
    //        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
    //        startActivityForResult(intent, PICKFILE_REQUEST_CODE);
    //    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PermissionsManager.SETTINGS_PERMISSION_CODE) {
            Navigation.findNavController(this, R.id.navHost).navigate(R.id.action_global_mainActivityFragment)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_settings) {
            Navigation.findNavController(this, R.id.navHost).navigate(R.id.action_global_settingsFragment)
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}