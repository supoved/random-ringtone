package com.pavelluzhetskiy.randomringtone.core

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.core.content.PermissionChecker.checkSelfPermission

class PermissionsManager {
    companion object {
        const val SETTINGS_PERMISSION_CODE = 173
        const val PERMISSION_UPDATED_CODE = 172
    }

    fun requestPhonePermission(activity: Activity){
        if (!checkPermission(activity, Manifest.permission.READ_PHONE_STATE)) {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.READ_PHONE_STATE), PERMISSION_UPDATED_CODE)
        }
    }

    fun requestPermissions(activity: Activity) {
        val missingPermissions = ArrayList<String>()
        if (!checkPermission(activity, Manifest.permission.READ_PHONE_STATE)) {
            Log.i("RandomRingtone", "missing permission: READ_PHONE_STATE")
            missingPermissions.add(Manifest.permission.READ_PHONE_STATE)
        }

        if (!checkSettingsPermission(activity)) {
            Log.i("RandomRingtone", "missing permission: WRITE_SETTINGS")
            val intent = Intent(ACTION_MANAGE_WRITE_SETTINGS)
            intent.data = Uri.parse("package:com.pavelluzhetskiy.randomringtone")
            startActivityForResult(activity, intent, SETTINGS_PERMISSION_CODE, null)
        }

        if (missingPermissions.size > 0) {
            ActivityCompat.requestPermissions(activity, missingPermissions.toTypedArray(), 172)
        }
    }

    fun checkPermission(context: Context, permission: String): Boolean {
        return checkSelfPermission(context, permission) == PERMISSION_GRANTED
    }

    fun checkSettingsPermission(context: Context): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return Settings.System.canWrite(context)
        }
        return checkPermission(context, Manifest.permission.WRITE_SETTINGS)
    }
}