package com.pavelluzhetskiy.randomringtone.core

import android.content.Context
import android.net.Uri
import android.provider.BaseColumns
import android.provider.MediaStore
import android.util.Log
import java.util.*

class RingtoneLibrary {
    private var ringtonePreferences: RingtonePreferences = RingtonePreferences()

    fun getAvailable(context: Context): List<Ringtone> {
        val folder = ringtonePreferences.getRingtoneFolder(context)
        return searchMediaLibrary(context, folder)
    }

    private fun searchMediaLibrary(context: Context, folder: String): List<Ringtone> {
        val parcialUri = Uri.parse("content://media/external/audio/media")
        val projection = arrayOf(BaseColumns._ID, MediaStore.Audio.Artists.ARTIST, MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.DATA)
        val where = MediaStore.Audio.Media.DATA + " LIKE ?"
        val params = arrayOf("%$folder%")
        val q = context.contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection, where, params, MediaStore.Audio.Media.TITLE)
        val ringtones = ArrayList<Ringtone>()
        try {
            while (q!!.moveToNext()) {
                val ringtoneID = q.getInt(q.getColumnIndex(MediaStore.MediaColumns._ID))
                val finalSuccessfulUri = Uri.withAppendedPath(parcialUri, "" + ringtoneID)
                val title = q.getString(2)
                val artist = q.getString(1)
                val path = q.getString(3)
                Log.i("RingtoneService", q.getString(0) + " " + title + " " + artist + " " + path)
                ringtones.add(Ringtone(ringtones.size, path, title, finalSuccessfulUri))
            }
        } finally {
            q!!.close()
        }
        return ringtones
    }
}