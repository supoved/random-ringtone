package com.pavelluzhetskiy.randomringtone.core

import android.app.*
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.pavelluzhetskiy.randomringtone.R
import com.pavelluzhetskiy.randomringtone.utils.Provider
import com.pavelluzhetskiy.randomringtone.workers.RemoveNotificationWorker
import java.util.*

class RingtoneManager {
    companion object {
        const val NOTIFICATION_ID = 1
        const val NOTIFICATION_CHANNEL_ID = "com.pavelluzhetskiy.ringtone"
    }

    var ringtoneLibrary: RingtoneLibrary = RingtoneLibrary()
    var ringtonePreferences: RingtonePreferences = RingtonePreferences()
    var notificationManagerProvider: Provider<NotificationManager>
    var alarmManagerProvider: Provider<AlarmManager>

    init {
        notificationManagerProvider = object : Provider<NotificationManager>() {
            override fun construct(context: Context): NotificationManager {
                return context.applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            }
        }
        alarmManagerProvider = object : Provider<AlarmManager>() {
            override fun construct(context: Context): AlarmManager {
                return context.applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            }
        }
    }

    fun setRingtone(context: Context, ringtoneIndex: Int): Boolean {
        val ringtone = ringtoneLibrary.getAvailable(context)[ringtoneIndex]
        return setRingtone(context, ringtone)
    }

    fun setRingtone(context: Context, ringtone: Ringtone?): Boolean {
        if (ringtone == null) return false
        Log.i("RingtoneService", "setRingtone: " + ringtone.title)
        try {
            RingtoneManager.setActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE, ringtone.mediaUri)
            ringtonePreferences.saveLastRingtonePosition(context, ringtone.id, ringtone.path)
        } catch (e: Exception) {
            Log.e("RingtoneService", "catch exception" + e.message)
            return false
        }
        return true
    }

    fun pickRandomRingtone(context: Context): Ringtone {
        val ringtones = ringtoneLibrary.getAvailable(context)
        val lastRingtone = ringtonePreferences.getLastRingtonePosition(context)
        if (lastRingtone.first > -1 && lastRingtone.first < ringtones.size && ringtones[lastRingtone.first].title == lastRingtone.second) {
            ringtones.drop(lastRingtone.first)
        }
        val generator = Random()
        val id = generator.nextInt(ringtones.size)

        return ringtones[id]
    }

    fun showNotification(context: Context, ringtone: String) {
        createNotificationChannel(context)
        notificationManagerProvider[context].notify(NOTIFICATION_ID, buildNotification(context, ringtone))
        RemoveNotificationWorker.enqueue(context)
    }

    fun buildNotification(context: Context, ringtone: String): Notification {
        return NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setPriority(NotificationManagerCompat.IMPORTANCE_LOW)
                .setSmallIcon(R.drawable.ic_stat_audiotrack)
                .setContentTitle(ringtone)
                .setContentIntent(PendingIntent.getActivity(context, 0, Intent(), 0))
                .setAutoCancel(true)
                .setContentText("New ringtone is set!")
                .build()
    }

    private fun createNotificationChannel(context: Context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) return
        val name = context.getString(R.string.channel_name)
        val descriptionText = context.getString(R.string.channel_description)
        val importance = NotificationManager.IMPORTANCE_LOW
        val channel = NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance).apply {
            description = descriptionText
        }

        // Register the channel with the system
        val notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    fun cancelNotification(context: Context) {
        notificationManagerProvider[context].cancel(NOTIFICATION_ID)
    }
}