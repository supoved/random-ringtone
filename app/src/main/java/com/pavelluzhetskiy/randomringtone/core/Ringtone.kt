package com.pavelluzhetskiy.randomringtone.core

import android.net.Uri

class Ringtone(var id: Int, var path: String, var title: String, var mediaUri: Uri)