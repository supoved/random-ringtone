package com.pavelluzhetskiy.randomringtone.core

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.os.Environment
import android.util.Pair
import androidx.preference.PreferenceManager
import com.pavelluzhetskiy.randomringtone.utils.Provider

class RingtonePreferences {
    private var preferencesProvider: Provider<SharedPreferences>
    private val permissionsManager: PermissionsManager = PermissionsManager()

    init {
        preferencesProvider = object : Provider<SharedPreferences>() {
            override fun construct(context: Context): SharedPreferences {
                return PreferenceManager.getDefaultSharedPreferences(context)
            }
        }
    }

    fun getRingtoneFolder(context: Context): String {
        return preferencesProvider[context].getString(PREFERENCES_SETTINGS_RINGTONE_FOLDER, Environment.DIRECTORY_RINGTONES)!!
    }

    fun getLastRingtonePosition(context: Context): Pair<Int, String> {
        val lastId = preferencesProvider[context].getInt(PREFERENCES_LAST_RINGTONE_ID, -1)
        val lastPath = preferencesProvider[context].getString(PREFERENCES_LAST_RINGTONE_PATH, "")!!
        return Pair(lastId, lastPath)
    }

    fun saveLastRingtonePosition(context: Context, position: Int, path: String) {
        preferencesProvider[context].edit()
                .putInt(PREFERENCES_LAST_RINGTONE_ID, position)
                .putString(PREFERENCES_LAST_RINGTONE_PATH, path)
                .apply()
    }

    fun getIsSwitchRingtoneAfterCallEnabled(context: Context): Boolean {
        val havePermission = permissionsManager.checkPermission(context, Manifest.permission.READ_PHONE_STATE)
        val isTurnedOn = preferencesProvider[context].getBoolean(PREFERENCES_SETTINGS_IS_SWITCH_ON_CALL_ENABLED, true)
        return havePermission && isTurnedOn
    }

    fun getTimerSwitch(context: Context): String {
        return preferencesProvider[context].getString(PREFERENCES_SETTINGS_SWITCH_TIMMER, "day")!!
    }

    companion object {
        const val PREFERENCES_LAST_RINGTONE_ID = "LAST_RINGTONE_ID"
        const val PREFERENCES_LAST_RINGTONE_PATH = "LAST_RINGTONE_PATH"
        const val PREFERENCES_SETTINGS_RINGTONE_FOLDER = "ringtone_folder_path"
        const val PREFERENCES_SETTINGS_IS_SWITCH_ON_CALL_ENABLED = "trigger_call"
        const val PREFERENCES_SETTINGS_SWITCH_TIMMER = "trigger_timer"
    }
}