package com.pavelluzhetskiy.randomringtone.utils

import android.content.Context

abstract class Provider<T> {
    private var cache: T? = null
    abstract fun construct(context: Context): T
    operator fun get(context: Context): T {
        if (cache == null) {
            cache = construct(context)
        }
        return cache!!
    }
}