package com.pavelluzhetskiy.randomringtone.workers

import android.content.Context
import android.util.Log
import androidx.work.*
import com.pavelluzhetskiy.randomringtone.core.RingtoneManager
import java.time.Duration
import java.util.concurrent.TimeUnit

class SetRandomRingtoneWorker(val context: Context, params: WorkerParameters) : Worker(context, params) {
    val ringtoneManager = RingtoneManager()

    override fun doWork(): Result {
        val ringtone = ringtoneManager.pickRandomRingtone(context)
        val result = ringtoneManager.setRingtone(context, ringtone)

        return if (result) {
            ringtoneManager.showNotification(context, ringtone.title)
            return Result.success()
        } else
            Result.failure()
    }

    companion object {
        const val ALARM_SWITCH_JOB_ID: String = "ALARM_SWITCH";

        fun enqueue(context: Context) {
            val uploadWorkRequest = OneTimeWorkRequestBuilder<SetRandomRingtoneWorker>().build()
            WorkManager.getInstance(context).enqueue(uploadWorkRequest)
        }

        fun enqueueTimer(context: Context, type: String) {
            if (type == "never") {
                Log.i("RingtoneService", "remove periodic ringtone switch")
                WorkManager.getInstance(context).cancelUniqueWork(ALARM_SWITCH_JOB_ID)
                return
            }

            Log.i("RingtoneService", "set periodic ringtone switch: $type")
            val workRequest = when (type) {
                "week" -> {
                    PeriodicWorkRequestBuilder<SetRandomRingtoneWorker>(7, TimeUnit.DAYS)
                            .setInitialDelay(7, TimeUnit.DAYS)
                            .build()
                }
                "day" -> {
                    PeriodicWorkRequestBuilder<SetRandomRingtoneWorker>(1, TimeUnit.DAYS)
                            .setInitialDelay(1, TimeUnit.DAYS)
                            .build()
                }
                else -> {
                    PeriodicWorkRequestBuilder<SetRandomRingtoneWorker>(1, TimeUnit.HOURS)
                            .setInitialDelay(1, TimeUnit.HOURS)
                            .build()
                }
            }

            WorkManager
                    .getInstance(context)
                    .enqueueUniquePeriodicWork(ALARM_SWITCH_JOB_ID, ExistingPeriodicWorkPolicy.REPLACE, workRequest)
        }
    }
}