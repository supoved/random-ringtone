package com.pavelluzhetskiy.randomringtone.workers

import android.content.Context
import androidx.work.*
import com.pavelluzhetskiy.randomringtone.core.RingtoneManager

class SetRingtoneWorker(val context: Context, params: WorkerParameters) : Worker(context, params) {
    val ringtoneManager = RingtoneManager()
    val index = params.inputData.getInt(EXTRA_RINGTONE_INDEX, -1)
    val title = params.inputData.getString(EXTRA_RINGTONE_TITLE) ?: "unknown"

    override fun doWork(): Result {
        val result = ringtoneManager.setRingtone(context, index)
        return if (result) {
            ringtoneManager.showNotification(context, title)
            Result.success()
        } else
            Result.failure()
    }

    companion object {
        const val EXTRA_RINGTONE_INDEX = "EXTRA_RINGTONE_INDEX"
        const val EXTRA_RINGTONE_TITLE = "EXTRA_RINGTONE_TITLE"
        fun enqueue(contexct: Context, index: Int, title: String) {
            val data = workDataOf(EXTRA_RINGTONE_INDEX to index, EXTRA_RINGTONE_TITLE to title)
            val uploadWorkRequest = OneTimeWorkRequestBuilder<SetRingtoneWorker>()
                    .setInputData(data)
                    .build()
            WorkManager.getInstance(contexct).enqueue(uploadWorkRequest)
        }
    }
}