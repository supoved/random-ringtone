package com.pavelluzhetskiy.randomringtone.workers

import android.content.Context
import androidx.work.*
import com.pavelluzhetskiy.randomringtone.core.RingtoneManager
import java.util.concurrent.TimeUnit

class RemoveNotificationWorker(val context: Context, params: WorkerParameters) : Worker(context, params) {
    val ringtoneManager = RingtoneManager()

    override fun doWork(): Result {
        ringtoneManager.cancelNotification(context)
        return Result.success()
    }

    companion object {
        fun enqueue(context: Context) {
            val uploadWorkRequest = OneTimeWorkRequestBuilder<RemoveNotificationWorker>()
                    .setInitialDelay(3, TimeUnit.MINUTES)
                    .build()
            WorkManager.getInstance(context)
                    .beginUniqueWork("cancel notification", ExistingWorkPolicy.REPLACE, uploadWorkRequest)
                    .enqueue()
        }
    }
}