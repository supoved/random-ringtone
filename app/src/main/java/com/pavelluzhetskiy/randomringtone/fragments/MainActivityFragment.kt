package com.pavelluzhetskiy.randomringtone.fragments

import android.content.SharedPreferences
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import androidx.preference.PreferenceManager
import com.pavelluzhetskiy.randomringtone.R
import com.pavelluzhetskiy.randomringtone.core.Ringtone
import com.pavelluzhetskiy.randomringtone.core.RingtoneLibrary
import com.pavelluzhetskiy.randomringtone.core.RingtonePreferences
import com.pavelluzhetskiy.randomringtone.core.RingtonePreferences.Companion.PREFERENCES_LAST_RINGTONE_ID
import com.pavelluzhetskiy.randomringtone.workers.SetRingtoneWorker


/**
 * A placeholder fragment containing a simple view.
 */
class MainActivityFragment : androidx.fragment.app.Fragment() {
    private var ringtons: List<Ringtone>? = null
    private var currentlyPlaying: Ringtone? = null
    private var currentlySelectedPath: String? = null

    private var mediaPlayer: MediaPlayer? = null
    private val ringtoneLibrary = RingtoneLibrary()
    private val ringtonePreferences = RingtonePreferences()
    private val changesListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
        if (context != null && key == PREFERENCES_LAST_RINGTONE_ID) {
            currentlySelectedPath = ringtonePreferences.getLastRingtonePosition(requireContext()).second
            ((requireView().findViewById<View>(R.id.ringtone_list) as ListView).adapter as RingtoneAdapter).notifyDataSetChanged()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onStart() {
        mediaPlayer = MediaPlayer()
        PreferenceManager.getDefaultSharedPreferences(requireContext()).registerOnSharedPreferenceChangeListener(changesListener)

        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        ringtons = ringtoneLibrary.getAvailable(requireContext())
        currentlySelectedPath = ringtonePreferences.getLastRingtonePosition(requireContext()).second
        (requireView().findViewById<View>(R.id.ringtone_list) as ListView).adapter = RingtoneAdapter()
    }

    override fun onStop() {
        super.onStop()
        PreferenceManager.getDefaultSharedPreferences(requireContext()).unregisterOnSharedPreferenceChangeListener(changesListener)
        mediaPlayer?.release()
        mediaPlayer = null
        currentlyPlaying = null
    }

    private inner class RingtoneAdapter : BaseAdapter() {
        // override other abstract methods here
        override fun getCount(): Int {
            return ringtons!!.size
        }

        override fun getItem(position: Int): Ringtone {
            return ringtons!![position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, container: ViewGroup): View {
            val ringtone = getItem(position)
            val isThisRingtonePlaying = currentlyPlaying != null && currentlyPlaying!!.path == ringtone.path
            val view = convertView
                    ?: layoutInflater.inflate(R.layout.ringtone_list_item, container, false)

            Log.i("RandomRingtone", "view item ${ringtone.id} path ${ringtone.path} playing: $isThisRingtonePlaying")

            view.findViewById<TextView>(R.id.item_path).text = ringtone.title

            val statusIconView = view
            if (currentlySelectedPath == ringtone.path) {
                statusIconView.setBackgroundResource(R.color.colorPrimaryLight)
            }else{
                statusIconView.setBackgroundResource(android.R.color.transparent)
            }

            val playView = view.findViewById<ImageView>(R.id.playIcon)
            if (isThisRingtonePlaying) {
                playView.setImageResource(android.R.drawable.ic_media_pause)
            } else {
                playView.setImageResource(android.R.drawable.ic_media_play)
            }

            playView.setOnClickListener {
                mediaPlayer?.apply {
                    if (isPlaying) {
                        reset()
                    }
                    if (currentlyPlaying == null || currentlyPlaying!!.path != ringtone.path) {
                        setAudioStreamType(AudioManager.STREAM_MUSIC)
                        setDataSource(requireContext(), ringtone.mediaUri)
                        prepare()
                        start()
                        currentlyPlaying = ringtone
                        Log.i("RandomRingtone", "currently playing ${currentlyPlaying!!.id} path ${currentlyPlaying!!.path}")
                        notifyDataSetChanged()
                        setOnCompletionListener {
                            reset()
                            currentlyPlaying = null
                            notifyDataSetChanged()
                        }
                    } else {
                        stop()
                        currentlyPlaying = null
                        notifyDataSetChanged()
                    }
                }
            }

            view.setOnClickListener {
                SetRingtoneWorker.enqueue(requireContext(), position, ringtone.title)
            }
            return view
        }
    }
}