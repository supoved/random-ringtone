package com.pavelluzhetskiy.randomringtone.fragments

import android.Manifest.permission.READ_PHONE_STATE
import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import com.pavelluzhetskiy.randomringtone.R
import com.pavelluzhetskiy.randomringtone.core.PermissionsManager
import com.pavelluzhetskiy.randomringtone.workers.SetRandomRingtoneWorker

class SettingsFragment : PreferenceFragmentCompat() {
    private val permissionsManager = PermissionsManager()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }

    override fun onResume() {
        super.onResume()

        val timerPreference: Preference = findPreference("trigger_timer")!!
        timerPreference.setOnPreferenceChangeListener { _, newValue ->
            SetRandomRingtoneWorker.enqueueTimer(requireContext(), newValue.toString())
            true
        }

        val phonePermissionPreference: SwitchPreference = findPreference("permission_phone")!!
        phonePermissionPreference.isChecked = permissionsManager.checkPermission(requireContext(), READ_PHONE_STATE)
        phonePermissionPreference.isEnabled = !permissionsManager.checkPermission(requireContext(), READ_PHONE_STATE)
        phonePermissionPreference.setOnPreferenceChangeListener { _, _ -> false }
        phonePermissionPreference.setOnPreferenceClickListener {
            permissionsManager.requestPhonePermission(requireActivity())
            true
        }

        val triggerCallPreference: Preference = findPreference("trigger_call")!!
        triggerCallPreference.isEnabled = permissionsManager.checkPermission(requireContext(), READ_PHONE_STATE)
    }
}