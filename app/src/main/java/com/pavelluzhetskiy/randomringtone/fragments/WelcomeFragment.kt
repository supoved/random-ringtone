package com.pavelluzhetskiy.randomringtone.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import com.pavelluzhetskiy.randomringtone.R
import com.pavelluzhetskiy.randomringtone.core.PermissionsManager

class WelcomeFragment : Fragment() {
    private val permissionsManager = PermissionsManager()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_welcome, container, false)

        view.findViewById<Button>(R.id.button_enable).setOnClickListener { v ->
            if (permissionsManager.checkSettingsPermission(this.requireContext())) {
                v.findNavController().navigate(R.id.mainActivityFragment)
            } else {
                permissionsManager.requestPermissions(this.requireActivity())
            }
        }

        return view
    }
}
