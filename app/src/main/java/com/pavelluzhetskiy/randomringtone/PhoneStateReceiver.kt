package com.pavelluzhetskiy.randomringtone

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager
import android.util.Log
import com.pavelluzhetskiy.randomringtone.core.RingtonePreferences
import com.pavelluzhetskiy.randomringtone.workers.SetRandomRingtoneWorker

class PhoneStateReceiver : BroadcastReceiver() {
    val preferences = RingtonePreferences()

    override fun onReceive(context: Context, intent: Intent) {
        val phoneState = intent.getStringExtra(TelephonyManager.EXTRA_STATE)
        Log.i("PhoneStateReceiver", phoneState)
        if(!preferences.getIsSwitchRingtoneAfterCallEnabled(context)){
            return
        }

        if (TelephonyManager.EXTRA_STATE_RINGING == phoneState) {
            SetRandomRingtoneWorker.enqueue(context)
        }
    }
}